<%-- 
    Document   : index
    Created on : 04-04-2020, 12:04:28
    Author     : kevin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
        <title>CALCULDORA DE INTERÉS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
    <!-- FRAMEWORK BOOTSTRAP-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    
    <style>
        body{
             background: linear-gradient(90deg, #4b6cb7 0%, #182848 100%);
        }
        #content{
            width: 70%;
            margin: 20px 32%;
        }
        .btn{
            width: 100%;
            font-size: 18px;
        }
    </style>
    
    </head>
    <body>
      <div id="content">
         <form name="form" action="controlador" method="POST">
         
             <div class="card w-50" style="background: white">
              <div class="card-body">
                <h2 class="card-title" style="text-align: center">CALCULDORA DE INTERÉS</h2>
                <div class="form-group">
              Ingrese Capital: <input type="number" min="0" step="any" class="form-control" placeholder="$" name="capital" required/>
                </div>
                 <div class="form-group">
              Ingrese porcentaje de tasa anual: <input type="number" min="0" step="any" class="form-control" placeholder="Tasa Anual" name="tasa" required/>
                </div>
                <div class="form-group">
                    Ingrese cantidad de años: <input type="number" min="0" step="any" class="form-control" placeholder="Años" name="year" required/>
                </div>
                <div style="margin: 0px auto">
                    <button type="submit" class="btn btn-primary">Calcular</button>
                </div>
              </div>
            </div>
      </div>
         
         </form>
        
    </body>
</html>
