/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.modelo;

/**
 *
 * @author kevin
 */
public class CalcularInteres {
    
    private float c;
    private float i;
    private float n;

    public CalcularInteres(float c, float i, float n) {
        this.c = c;
        this.i = i;
        this.n = n;
    }
    
    public float Interes() {
       float interes = c * (i/100) * n;
       return interes;
    }
}
