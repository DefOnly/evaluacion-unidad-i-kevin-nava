<%-- 
    Document   : calculo
    Created on : 04-04-2020, 12:06:04
    Author     : kevin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="root.modelo.CalcularInteres"%>
<%
    CalcularInteres calculo1 = (CalcularInteres) request.getSession().getAttribute("primero");
%>
  
<!DOCTYPE html>
<html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        
        <title>Calculo de Interés</title>
        
        <style>
            body{
             background: linear-gradient(90deg, #4b6cb7 0%, #182848 100%);
        }
        </style>
    </head>
    <body> 
         <div class="jumbotron jumbotron-fluid">
              <div class="container">
                 <p class="lead">Su interés simple producido es de:</p>
                <h1 class="display-4">$<%=calculo1.Interes()%></h1>
              </div>
          </div>
    </body>
</html>
